FROM python:3.12-slim

RUN install -d -m 0755 /etc/apt/keyrings
RUN apt update && apt install -y wget locales

# Instal Firefox
RUN wget -q https://packages.mozilla.org/apt/repo-signing-key.gpg -O- | tee /etc/apt/keyrings/packages.mozilla.org.asc > /dev/null
RUN echo "deb [signed-by=/etc/apt/keyrings/packages.mozilla.org.asc] https://packages.mozilla.org/apt mozilla main" | tee -a /etc/apt/sources.list.d/mozilla.list > /dev/null
RUN apt update
RUN apt install -y firefox

# Install python libraries
COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

# Set french locale
RUN echo "Europe/Paris" > /etc/timezone && \
    dpkg-reconfigure -f noninteractive tzdata && \
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    sed -i -e 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen && \
    echo 'LANG="fr_FR.UTF-8"'>/etc/default/locale && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=fr_FR.UTF-8

# Install geckodriver
RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.34.0/geckodriver-v0.34.0-linux64.tar.gz &&\
    tar -xzf geckodriver-v0.34.0-linux64.tar.gz 

# Copy source
ADD src/ /app/
RUN mv /geckodriver /app

WORKDIR /app

CMD ["python", "-u", "bot.py"] 