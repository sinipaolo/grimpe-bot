import locale
from datetime import datetime, timedelta
import jinja2
import discord
from grimpe_info_retriever import GrimpeInfoRetriever
from models import GrimpeEvent

class GrimpeFormatter:
    """Class used to parse all known events around the Nice Allianz Riviera
    and format messages intended for discord 
    """

    def __init__(self) -> None:
        self.info_retriever = GrimpeInfoRetriever(headless=True)
        self.jinja_env = jinja2.Environment(
            loader=jinja2.FileSystemLoader('message_templates/')
        )
        self.embed_color = 0xAB47BC
        locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")

    def retrieve_events(self):
        """Method to update the list of available events from the scrapers
        """
        self.events = self.info_retriever.scrape_all_events() 

    def get_events_in_period(
            self,
            start_datetime: datetime,
            end_datetime: datetime
    ) -> list[GrimpeEvent]:
        """Get a list of all events happening in between two dates

        Args:
            start_datetime (datetime): Start datetime
            end_datetime (datetime): End datetime

        Returns:
            list[GrimpeEvent]: List of events happening in between the two dates
        """
        
        self.retrieve_events()
        events_in_period = []
        for event in self.events:    
            if start_datetime < event.date < end_datetime:
                events_in_period.append(event)

        # Return events sorted in chronological order
        return sorted(events_in_period, key=lambda x: x.date)
    
    def get_events_embed(self) -> discord.Embed:
        """Formats a nice-looking message from events in the next seven days.

        Returns:
            discord.Embed: Message intended for publication on Discord
        """

        now = datetime.now()
        events_this_week = self.get_events_in_period(
            now,
            now + timedelta(days=8)
        )

        # Use Jinja2 to template the message
        template = self.jinja_env.get_template('planning.jinja2')
        events_embed = discord.Embed(
            title="Évènements dans les 7 prochains jours",
            description=template.render({"events": events_this_week}),
            color=self.embed_color
        )

        return events_embed
    
    def get_traffic_map_embed(self) -> tuple[discord.Embed, discord.File]:
        """Scrape all traffic sources and return an embed formatting them

        Returns:
            discord.Embed: The embed object containing traffic information
        """

        trafficGrimpeMedia = self.info_retriever.scrape_traffic()[0]
        template = self.jinja_env.get_template('traffic-map.jinja2')

        traffic_image = discord.File(
            fp=trafficGrimpeMedia.file_path,
            filename='traffic.png'
        )
        traffic_embed = discord.Embed(
            title="Traffic Grimpe",
            description=template.render({
                "maps_url": trafficGrimpeMedia.source_url,
                "url_text": trafficGrimpeMedia.url_text
            }),
            color=self.embed_color
        )

        traffic_embed.set_image(
            url=f"attachment://{trafficGrimpeMedia.file_path}"
        )

        return traffic_embed, traffic_image