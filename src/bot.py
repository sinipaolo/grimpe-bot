import os
from datetime import datetime
import croniter
import discord
from discord import app_commands
from discord.ext import tasks
from grimpe_formatter import GrimpeFormatter

# Discord IDs
update_channel_id = int(os.environ.get("UPDATE_CHANNEL_ID"))
discord_token = os.environ.get('DISCORD_TOKEN')

# Discord setup
intents = discord.Intents.default()
intents.message_content = True
intents.guilds = True
client = discord.Client(intents=intents)
tree = app_commands.CommandTree(client)

# Scheduled Messages setup
schedule = '0 11 * * 1' # Midday on mondays
cron = croniter.croniter(schedule, datetime.now())
next_update_time = cron.get_next(datetime)

grimpe_formatter = GrimpeFormatter()

@client.event
async def on_ready():
    """Entrypoint function called at bot start-up
    """

    await tree.sync()
    weekly_update.start()
    print(f'We have logged in as {client.user}')

@tasks.loop(minutes=1)
async def weekly_update():
    """Loop Function handling the automated messages
    """
    global next_update_time
    # If it the update datetime has not been reached, return early
    if next_update_time > datetime.now():
        return
    
    # Generate the next update time
    next_update_time = cron.get_next(datetime)

    # Send the automated message
    update_channel = client.get_channel(update_channel_id)
    await update_channel.send(
        embed=grimpe_formatter.get_events_embed()
    )

@tree.command(
    name="grimpe-planning",
    description="Récupérer les events autour de l'Allianz Riviera"
)
async def get_planning(interaction):
    """Function setting up the manual command to retrieve the schedule of events
    """
    # Getting planning info can take a long time, so need to defer the response
    await interaction.response.defer()
    await interaction.followup.send(
        embed=grimpe_formatter.get_events_embed()
    )

@tree.command(
    name="grimpe-traffic",
    description="Afficher une image du traffic autour de l'Allianz Riviera"
)
async def get_traffic_map(interaction):
    """Function setting up the manual command to show a live map of the traffic
    as an embed
    """
    # Getting traffic info can take a long time, so need to defer the response
    await interaction.response.defer()
    traffic_embed, traffic_image = grimpe_formatter.get_traffic_map_embed()
    await interaction.followup.send(
        file=traffic_image,
        embed=traffic_embed
    )

client.run(discord_token)
