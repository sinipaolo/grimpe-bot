from models.grimpe_item import GrimpeItem
import datetime

class GrimpeEvent(GrimpeItem):

    name: str
    location: str
    date: datetime.datetime

    def __str__(self):
        return f"{self.name} at {self.location}, the {self.date}"
