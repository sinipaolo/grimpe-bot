from os import PathLike
from typing import Optional
from models.grimpe_item import GrimpeItem

class GrimpeMedia(GrimpeItem):

    file_path: PathLike
    source_url: Optional[str]
    url_text: Optional[str]