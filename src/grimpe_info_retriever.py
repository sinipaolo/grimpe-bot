from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from models import GrimpeEvent, GrimpeMedia
from scrapers import NikaiaScraper, AllianzRivieraScraper, TrafficMapScraper

class GrimpeInfoRetriever:
    """Class used to scrape websites to gather information about events
    around the Nice Allianz Riviera stadium
    """

    def __init__(self, headless=False) -> None:

        options = Options()
        if headless:
            options.add_argument("--headless")

        options.add_argument("--width=2560")
        options.add_argument("--height=1440")

        driver = webdriver.Firefox(
            options=options
        )

        # Separate scrapers according to which command they will be used for
        self.scrapers = {
            "events": [
                NikaiaScraper(driver=driver),
                AllianzRivieraScraper(driver=driver)
            ],
            "traffic": [
                TrafficMapScraper(driver=driver)
            ]
        }
    
    def scrape_all_events(self) -> list[GrimpeEvent]:
        """Scrape all registered websites for events.
        Add a scraper class to self.scrapers["event"] to have it scraped
        by this method.

        Returns:
            list[GrimpeEvent]: List of events from all registered scrapers
        """
        events = []

        for scraper in self.scrapers["events"]:
            events.extend(scraper.scrape())

        return events
    
    def scrape_traffic(self) -> list[GrimpeMedia]:
        """Scrape all registered websites for traffic.
        Add a scraper class to self.scrapers["traffic"] to have it scraped
        by this method.

        Returns:
            list[GrimpeEvent]: List of events from all registered scrapers
        """
        media = []

        for scraper in self.scrapers["traffic"]:
            media.extend(scraper.scrape())

        return media
    


if __name__ == "__main__":

    grimpe_scraper = GrimpeInfoRetriever()
    grimpe_scraper.get_traffic_map()
