from models import GrimpeMedia
from scrapers.scraper_abc import GrimpeInfoRetriever

class TrafficMapScraper(GrimpeInfoRetriever):

    def __init__(self, driver) -> None:
        super().__init__(
            driver=driver,
            scraper_target="traffic"
        )
        self.screenshot_path = "traffic.png"

    def scrape(self) -> list[GrimpeMedia]:

        self.driver.get(self.target_url)

        try:
            self.driver.find_element(
                "xpath",
                "//span[contains(text(), 'Tout refuser')]"
            ).click()
        except:
            pass

        self.driver.get_screenshot_as_file(self.screenshot_path)

        return [GrimpeMedia(
            file_path=self.screenshot_path,
            source_url=self.target_url,
            url_text="Carte en Direct"
        )]
    
