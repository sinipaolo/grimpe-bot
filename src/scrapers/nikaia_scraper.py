from datetime import datetime
from models import GrimpeEvent
from scrapers.scraper_abc import GrimpeInfoRetriever

class NikaiaScraper(GrimpeInfoRetriever):

    def __init__(self, driver) -> None:
        super().__init__(
            driver=driver,
            scraper_target="nikaia"
        )

    def scrape(self) -> list[GrimpeEvent]:
        """Go to Nikaïa programmation page and parse
        every event there

        Returns:
            list[GrimpeEvent]: List of every available Nikaïa event online
        """
        events = []

        self.driver.get(self.target_url)
        # Divs containing event info
        infos = self.driver.find_elements(
            "xpath",
            "//div[@class='infosholder']"
        )

        for info in infos:

            event_name = info.find_element(
                "xpath",
                "h1"
            ).get_attribute('innerText')

            event_date = datetime.strptime(
                info.find_element(
                    "xpath",
                    "p/time"
                ).get_attribute('datetime'),
                '%Y-%m-%dT%H:%M'
            )

            events.append(
                GrimpeEvent(
                    name=event_name,
                    location="Nikaïa",
                    date=event_date
                )
            )

        return events