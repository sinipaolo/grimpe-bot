from datetime import datetime
from models import GrimpeEvent
from scrapers.scraper_abc import GrimpeInfoRetriever

class AllianzRivieraScraper(GrimpeInfoRetriever):

    def __init__(self, driver) -> None:
        super().__init__(
            driver=driver,
            scraper_target="ogcnice"
        )

    def scrape(self) -> list[GrimpeEvent]:
        """Go to OGC Nice programmation page and parse
        every event there

        Returns:
            list[GrimpeEvent]: List of every available OGC Nice match online
        """
        events = []

        self.driver.get(self.target_url)

        infos = self.driver.find_elements(
            "xpath",
            "//div[contains(@class, 'matchCardContent')]"
        )

        for info in infos:

            match_name = info.find_element(
                "xpath",
                "h2"
            ).get_attribute('innerText')
            match_name = match_name.replace("\n", " ").strip()

            match_date = datetime.strptime(
                info.find_element(
                    "xpath",
                    "p/time"
                ).get_attribute("datetime"),
                '%Y-%m-%d %H:%M:%S.%f%z'
            ).replace(tzinfo=None)

            events.append(
                GrimpeEvent(
                    name=match_name,
                    location="Allianz Riviera",
                    date=match_date
                )
            )

        return events