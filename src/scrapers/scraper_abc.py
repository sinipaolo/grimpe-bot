from os import PathLike
import json
from abc import ABC, abstractmethod
from selenium.webdriver.remote.webdriver import WebDriver
from models import GrimpeItem

class GrimpeInfoRetriever(ABC):

    def __init__(
        self,
        driver: WebDriver,
        scraper_target: str,
        urls_file: PathLike = "urls.json"
    ) -> None:
        super().__init__()
        self.driver = driver
        self.scraper_target = scraper_target
        self.urls_file = urls_file
        self.target_url = self.get_source_url()

    def get_source_url(self) -> str:
        """Go through the urls_file and return the target URL for this scraper

        Returns:
           str: URL
        """

        with open(self.urls_file, 'r') as urls_file:
            result = json.loads(urls_file.read())

        return result[self.scraper_target]

    @abstractmethod
    def scrape(self) -> list[GrimpeItem]:
        return