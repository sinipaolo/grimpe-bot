# Grimpe Bot

Bot pour avoir des infos sur les évènements autour du stade Allianz Riviera de Nice.

## Lancement

Build l'image:
```sh
docker build -t grimpebot .
```

Lancer le container:
```sh
docker run \
    -e DISCORD_TOKEN=${DISCORD_TOKEN} \
    -e UPDATE_CHANNEL_ID=${UPDATE_CHANNEL_ID} \
    -it grimpebot
```
Où:
- $DISCORD_TOKEN est le token du bot à utiliser
- $UPDATE_CHANNEL_ID est l'ID du channel où les évènements sont postés
  automatiquement une fois par semaine


## Usage

Le bot répond à:
```shell
/grimpe-planning
```

Rendu exemple:
```
Évènements dans les 7 prochains jours:
    Mardi 05 Mars 20:00 LORDS OF THE SOUND à Nikaïa
    Mercredi 06 Mars 20:00 AC/DC TRIBUTE SHOW à Nikaïa
    Vendredi 08 Mars 21:00 OGC NICE vs MONTPELLIER HSC à Allianz Riviera
    Samedi 09 Mars 20:30 L'HÉRITAGE GOLDMAN à Nikaïa
```